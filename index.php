<?php
require('Animal.php');
require('Frog.php');
require('Ape.php');

$sheep = new Animal("shaun");
echo 'Nama :'.$sheep->get_name().'<br>'; // "shaun"
echo 'Legs :'.$sheep->get_legs().'<br>'; // 4
echo 'Blood :'.$sheep->get_cold_blooded().'<br>'; // "no"


$kodok = new Frog("buduk");
echo 'Nama :'.$kodok->get_name().'<br>'; // "shaun"
echo 'Legs :'.$kodok->get_legs().'<br>'; // 4
echo 'Blood :'.$kodok->get_cold_blooded().'<br>'; // "no"
echo 'Jump :'.$kodok->jump(). '<br>' ; // "hop hop"

$sungokong = new Ape("kera sakti");
echo 'Nama :'.$sungokong->get_name().'<br>'; // "shaun"
echo 'Legs :'.$sungokong->get_legs().'<br>'; // 4
echo 'Blood :'.$sungokong->get_cold_blooded().'<br>'; // "no"
echo 'Yell :'.$sungokong->yell(). '<br>'; // "Auooo"

?>