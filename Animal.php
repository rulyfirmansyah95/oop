<?php




class Animal{
	public $nama;
	public $legs;
	public $cold_blooded;

	function __construct($nama,$legs=4,$blood="no"){
		$this->nama=$nama;
		$this->legs=$legs;
		$this->cold_blooded=$blood;
	}	

	function get_name(){
		return $this->nama;
	}

	function get_legs(){
		return $this->legs;
	}

	function get_cold_blooded(){
		return $this->cold_blooded;
	}

}
// NB: Boleh juga menggunakan method get 

?>